class Employee < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]
  
  validates :name, :surname, :dob, :job_title, :hometown, :experience, presence: true
  validates :hometown, inclusion: { in: ['Baku', 'Moscow', 'London'] }
  validates :experience, inclusion: { in: ['1-3 years', '3-5 years', '5+ years'] }

  def full_name
    [name, surname].join(' ')
  end

  private
    def should_generate_new_friendly_id?
      slug.blank? || name_changed? || surname_changed? || job_title_changed?
    end
    def slug_candidates
    [
      [:name, :surname, :job_title]
    ]
    end
    
end
