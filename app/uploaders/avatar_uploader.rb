class AvatarUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  storage :file

  process resize_to_fit: [400, 400]

  version :thumb do
    process resize_to_fill: [200,200]
  end

  def extension_whitelist
    %w(jpg jpeg png)
  end

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url(*args)
    "/fallback/" + [model.class.to_s.underscore, "avatar_default.png"].compact.join('_')
  end

end
