class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :surname
      t.date :dob
      t.string :job_title
      t.string :hometown
      t.string :experience

      t.timestamps
    end
  end
end
