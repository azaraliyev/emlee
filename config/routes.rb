Rails.application.routes.draw do
  devise_for :users,
  path_names: {
    sign_in: 'login',
    sign_out: 'logout',
  },
  controllers: { 
    sessions: "users/sessions",
  }
  root 'pages#index'

  get '/about', to: 'pages#about'

  resources :employees
end
