module ComponentHelper
  def component(name, data: {}, &block)
    render_component("components/#{name}", { data: data }, &block)
  end

  private

  def render_component(name, locals, &block)
    if block_given?
      render layout: name, locals: locals, &block
    else
      render partial: name, locals: locals
    end
  end
end